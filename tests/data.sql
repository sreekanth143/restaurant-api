INSERT INTO orders (order_id, customer_id, status, order_time)
VALUES
  (1, 1, 'Cancelled', 2023-01-05 13:04:27.612994);

INSERT INTO ordered_items (order_id, item_name, quantity, size)
VALUES
  (1, 'Chicken Biryani', 4, 'Full');